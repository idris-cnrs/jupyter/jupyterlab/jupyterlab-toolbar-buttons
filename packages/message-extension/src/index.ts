import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
} from '@jupyterlab/application';

import { IToolbarWidgetRegistry } from '@jupyterlab/apputils';

import { PageConfig, URLExt } from '@jupyterlab/coreutils';

import { ServerConnection } from '@jupyterlab/services';

import { Widget } from '@lumino/widgets';

import '../style/index.css';

const TOPBAR_TEXT = 'jp-TopBar-Text';

/**
 * Initialization data for the jupyterlab-topbar-message extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: '@idriscnrs/topbar-message:entry',
  autoStart: true,
  requires: [IToolbarWidgetRegistry],
  activate: async (
    app: JupyterFrontEnd,
    toolbarRegistry: IToolbarWidgetRegistry
  ) => {
    console.log('@idriscnrs/topbar-message extension is activated!');
    // server connection settings (such as headers) _can't_ be a global, as they
    // can potentially be configured by other extensions
    const serverSettings = ServerConnection.makeSettings();

    // Fetch configured server processes from {base_url}/topbar/message
    const url = URLExt.join(PageConfig.getBaseUrl(), 'topbar/message');
    const response = await ServerConnection.makeRequest(
      url,
      {},
      serverSettings
    );

    if (!response.ok) {
      console.log('Could not fetch topbar message');
      console.log(response);
      return;
    }
    const data = await response.json();
    const text = data.message;

    // Create text element
    const textNode = document.createElement('div');
    textNode.textContent = text;

    toolbarRegistry.addFactory('TopBar', 'text', () => {
      const textWidget = new Widget({ node: textNode });
      textWidget.addClass(TOPBAR_TEXT);
      return textWidget;
    });
  },
};

export default extension;
