import os
import json

from traitlets import Unicode
from traitlets.config import Configurable

from tornado import web
from jupyter_server.base.handlers import JupyterHandler
from jupyter_server.utils import url_path_join as ujoin

from ._version import __version__  # noqa


class TopBarMessageConfig(Configurable):
    """Allows configuration of topbar message"""

    message = Unicode(
        '',
        help="""Message to be displayed in the JupyterLab Topbar area
        
        Loaded from the TOPBAR_MESSAGE env variable by default.
        """
    ).tag(config=True)

    def _message_default(self):
        return os.environ.get('TOPBAR_MESSAGE', '')
    

class TopBarMessage(JupyterHandler):
    """Handler to send the message that will be displayed in the topbar"""
    
    def initialize(self, message):
        self.message = message
    
    @web.authenticated
    async def get(self):
        payload = {
            'message': self.message
        }
        self.finish(json.dumps(payload))


def _jupyter_server_extension_points():
    return [{"module": "jupyterlab_topbar_message"}]


def _jupyter_labextension_paths():
    return [{
        "src": "labextension",
        "dest": "@idriscnrs/topbar-message"
    }]


def load_jupyter_server_extension(nbapp):
    """
    Called when the extension is loaded.

    Args:
        nbapp : handle to the Notebook webserver instance.
    """
    config = TopBarMessageConfig(parent=nbapp)
    message = config.message

    web_app = nbapp.web_app
    base_url = web_app.settings["base_url"]

    web_app.add_handlers(".*$", [
        (ujoin(base_url, 'topbar/message'), TopBarMessage, {'message': message}),
    ])
