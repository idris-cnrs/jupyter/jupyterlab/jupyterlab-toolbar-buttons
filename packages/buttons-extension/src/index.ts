import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
} from '@jupyterlab/application';

import { URLExt } from '@jupyterlab/coreutils';

import { IToolbarWidgetRegistry } from '@jupyterlab/apputils';

import { Widget } from '@lumino/widgets';

import '@jupyterlab/application/style/buttons.css';

const extension: JupyterFrontEndPlugin<void> = {
  id: '@idriscnrs/topbar-buttons:entries',
  autoStart: true,
  requires: [IToolbarWidgetRegistry, JupyterFrontEnd.IPaths],
  activate: async (
    app: JupyterFrontEnd,
    toolbarRegistry: IToolbarWidgetRegistry,
    paths: JupyterFrontEnd.IPaths
  ): Promise<void> => {
    console.log('@idriscnrs/topbar-buttons - extension is activated!');

    // Create a map with announcement and documentation links
    const buttonLinks: {
      [key: string]: string;
    } = {
      Announcement: '/services/announcement/',
      Docs: 'https://idris-cnrs.gitlab.io/jupyter/jupyter-documentation/',
      HubCtlPlane: URLExt.join(paths.urls.hubPrefix || '', 'home'),
      LogOut: URLExt.join(paths.urls.hubPrefix || '', 'logout'),
    };

    // Create button elements
    for (const link of Object.keys(buttonLinks)) {
      const buttonNode = document.createElement('div');
      if (link === 'LogOut') {
        buttonNode.innerHTML = `<a href="${buttonLinks[link]}">${link}</a>`;
      } else {
        buttonNode.innerHTML = `<a href="${buttonLinks[link]}" target="_blank">${link}</a>`;
      }

      toolbarRegistry.addFactory('TopBar', link, () => {
        const buttonWidget = new Widget({ node: buttonNode });
        buttonWidget.addClass('jp-TopBar-Buttons');
        buttonWidget.addClass('jp-Button-flat');
        return buttonWidget;
      });
    }
  },
};

export default extension;
